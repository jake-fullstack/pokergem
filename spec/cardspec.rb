require 'rspec'
require 'poker/card'

describe "Card" do
  it 'names itself' do
  	c = Card.new(:ace, :spades)
  	c.to_s.should eq 'ace of spades'
  end
end
