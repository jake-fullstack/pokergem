Gem::Specification.new do |s|
  s.name        = 'poker'
  s.version     = '0.0.33'
  s.date        = '2013-12-04'
  s.summary     = "Objects for running poker games in Ruby"
  s.description = "A library of classes and objects for running a poker game in Ruby."
  s.authors     = ["Jake Bladt"]
  s.email       = 'jake@full-stack.net'
  s.files       = Dir.glob ['lib/poker.rb', 'lib/poker/**/*.rb'] # `git ls-files`.split("\n")
  s.homepage    = 'http://rubygems.org/gems/poker'
  s.license     = 'MIT'

  # Other gems
  s.add_development_dependency('rspec', [">= 2.10.0"])
end